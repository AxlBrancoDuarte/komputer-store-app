//DOM Elements
const bankBalanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("loan");
const loanButtonElement = document.getElementById("loan-button");
const payElement = document.getElementById("pay");
const workButtonElement = document.getElementById("work-button");
const bankButtonElement = document.getElementById("bank-button");
const payOffLoanButtonElement = document.getElementById("pay-off-loan-button");
const buyNowButtonElement = document.getElementById("computer-buy");
const computerNameElement = document.getElementById("computer-title");


//EventListeners
loanButtonElement.addEventListener("click", requestLoan);
workButtonElement.addEventListener("click", recivePay);
bankButtonElement.addEventListener("click", bankPay);
payOffLoanButtonElement.addEventListener("click", payOffLoan);
buyNowButtonElement.addEventListener("click", buyLaptop);

//Functions

//Requests a loan via a Prompt that converts the entered amount to a int.
function requestLoan() {
    let loan = +prompt("Please enter requested loan ammount:", "0");
    if (loan <= bank.balance * 2 && bank.outstandingLoan == "") {
        let newBalance = bank.balance + loan;
        bank.balance = newBalance;
        bank.outstandingLoan = loan;
        updateBalances();
    } else {
        alert(`You do not currently qualify for a loan of:  €${loan}`);
    };
};

//Deposits pay into bank with a 10% cut paying off the loan
function bankPay() {
    if (bank.outstandingLoan >= 1) {
        let loanPayment = work.pay * .1; //10% cut to pay loan
        if (loanPayment > bank.outstandingLoan) {
            bank.balance += getdifference(loanPayment, bank.outstandingLoan) //makes sure loan is not over paid.
            bank.outstandingLoan = 0;
            work.pay = 0;
        } else {
            bank.outstandingLoan -= loanPayment;
            bank.balance += work.pay - loanPayment;
            work.pay = 0;
        }
    } else {
        bank.balance += work.pay;
        work.pay = 0;
    };
    updateBalances();
};

function recivePay() {
    work.pay += 100;
    updateBalances();
};

//Uses pay to pay off loans in full with the excess being banked to balance.
function payOffLoan() {
    if (work.pay === 0) {
        alert("Error: Not funds in pay.");
    } else if (work.pay > bank.outstandingLoan) {
        bank.balance += getdifference(work.pay, bank.outstandingLoan)
        bank.outstandingLoan = 0;
        work.pay = 0;
    } else {
        bank.outstandingLoan -= work.pay;
        work.pay = 0;
    }
    updateBalances()
}

//Check to see if you can afford and buys a laptop
function buyLaptop() {
    const computerPriceElement = document.getElementById("pc-price");
    let laptopPrice = computerPriceElement.innerText;
    let computerName = computerNameElement.innerText;
    console.log(laptopPrice)
    if (bank.balance >= laptopPrice) {
        bank.balance -= laptopPrice;
        alert(`Congradulation on purchasing your new ${computerName} system`)
        updateBalances()
    } else {
        alert(`Unfortunalty you do not have enough funds to purchase the ${computerName} system`)
    }
}

//Updates the dom elements with stored values
function updateBalances() {
    outstandingLoanElement.innerText = `€${bank.outstandingLoan}`;
    bankBalanceElement.innerText = `€${bank.balance}`;
    payElement.innerText = `€${work.pay}`;
    toggleHiddenLoan()
};

//Toggles hidden elements
function toggleHiddenLoan() {
    const loanElement = document.getElementById("loan-element");
    const payOffElement = document.getElementById("pay-off-loan-button");

    if (bank.outstandingLoan >= 1) {
        loanElement.classList.remove("hidden");
        payOffElement.classList.remove("removed");
    } else {
        loanElement.classList.add("hidden");
        payOffElement.classList.add("removed");

    }
}

function getdifference(a, b) {
    return Math.abs(a - b);
}


//Objects for storing Bank and work values.
const bank = {
    balance: 0,
    outstandingLoan: 0,
};

const work = {
    pay: 0,
};
