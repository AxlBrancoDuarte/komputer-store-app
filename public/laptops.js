// DOM Elements
const computerSelectElement = document.getElementById("computer");
const computerTitleElement = document.getElementById("computer-title");
const computerImageElement = document.getElementById("computer-image");
const computerSpecsElement = document.getElementById("computer-specs");
const computerBuyElement = document.getElementById("computer-buy")
const computerDescriptionElement = document.getElementById("computer-description")
const computerPriceElement = document.getElementById("computer-price")

//Error handeler should there be no image avalible
document.getElementById("computer-image").addEventListener("error", () => {
    computerImageElement.src = "https://demofree.sirv.com/nope-not-here.jpg";
});

//Fetch request
async function fetchComputers() {
    try {
        const response = await fetch(
            "https://noroff-komputer-store-api.herokuapp.com/computers"
        );
        const json = await response.json(); // JavaScript Object Notation
        return json; //  Computers
    } catch (error) {
        console.error(error.message);
    }
}

//Variable to hold fetched array
const computers = await fetchComputers();

//Add computers to the HTML select element
for (const computer of computers) {
    const html = `<option value=${computer.id}>${computer.title}</option>`;
    computerSelectElement.insertAdjacentHTML("beforeend", html);
}

//When select option changes sets and calls the render for selected computer
function onSelectChange() {
    const computerId = this.value;
    const computer = computers.find(computer => computer.id === +computerId);
    console.log(computer)
    renderSelectedComputer(computer);
    computerBuyElement.classList.remove("hidden");
}
computerSelectElement.addEventListener("change", onSelectChange);

//Renders selected computer
const renderSelectedComputer = (computer) => {
    computerTitleElement.innerText = computer.title;
    computerImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${computer.image}`;
    computerDescriptionElement.innerText = computer.description;
    computerPriceElement.innerHTML = `<span id="pc-price">${computer.price}</span> Euros`;
    computerSpecsElement.innerHTML = "";
    for (const spec of computer.specs) {
        computerSpecsElement.insertAdjacentHTML(
            "beforeend",
            `<li>${spec}</li>`
        );
    }
};
