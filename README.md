<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="public/images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Komputer Store App</h3>

  <p align="center">
    Build a dynamic webpage using “vanilla” JavaScript.
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#functionality">Functionality</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

 <a href="https://github.com/github_username/repo_name">
    <img src="public/images/app.png" alt="app">
  </a>

<p>This is a my first Assignment Project for the Noroff Java Bootcamp.
Where we were tasked with creating a minor app that consisted of a wallet(bank) and income system that you can use to purchase a computers supplied through a RESTapi.</p>

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [JS](https://www.javascript.com/)
* [HTML5](https://developer.mozilla.org/en-US/docs/Glossary/HTML5)
* [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->




<!-- USAGE EXAMPLES -->
## Functionality

<p>Bank:</p>
<ul>
<li>Stores money you have deposited.</li>
<li>Can request loan.</li>
<li>Can only have one loan.</li>
</ul>
<p>Work:</p>
<ul>
<li>Stores any money you have earned from working</li>
<li>Can deposite earned money to bank where 10% will go to current loan.</li>
<li>Can pay off loan using all earned money, any over money will go to bank balance.</li>
</ul>
<p>Computer:</p>
<ul>
<li>Can browse a few computers.</li>
<li>Can use bank balance to purchase selected computer.</li>
</ul>
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->






<!-- CONTRIBUTING -->






<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Axl Branco Duarte - axl.nl@hotmail.com
Project Link: [https://github.com/github_username/AxlBrancoDuarte](https://github.com/github_username/repo_name)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
